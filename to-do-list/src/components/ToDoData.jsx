const ToDoData = [
        {
            id: 1,
            text: "Grocery Shopping",
        },
        {
            id: 2,
            text: "Do the Dishes",
        },
        {
            id: 3,
            text: "Pilates",
        },
        {
            id: 4,
            text: "Do Homework",
        }
    ]

  export default ToDoData;