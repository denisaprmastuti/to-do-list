import React from "react";
import "./ToDoItem.css";

function ToDoItem(props) {
    return(
        <div className="todoList">
            <input 
                type="checkbox" 
                checked={props.item.finished} 
                onChange={() => props.handleChange(props.item.id)} />
            <span>{props.item.text}</span>
        </div>
    )
}
export default ToDoItem