import React from 'react';
import './ToDo.css'
import ToDoItem from './ToDoItem';
import ToDoData from './ToDoData'

class ToDo extends React.Component {
    constructor() {
        super()
        this.state = {
            todo: ToDoData
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(id) {
        this.setState(prevState => {
            const updateTodo = prevState.todo.map(todo => {
                if (todo.id === id) {
                    todo.finished = !todo.finished
                }
                return todo
            })
            return {
                todo: updateTodo
            }
        })

    }

    render() {
        const todoItem = this.state.todo.map(item => <ToDoItem key={item.id} item={item} handleChange={this.handleChange}/>)
            return (
                <div className="todo-list">
                    <h2>My To Do List for Today:</h2>
                    {todoItem}
                </div>
            )
    }
}


export default ToDo;