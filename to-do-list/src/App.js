import './App.css';
import Todo from './components/todo';

function App() {
  return (
    <body>
      <Todo />
    </body>
  );
}

export default App;
